const axios = require('axios')
const apiUrl = 'https://wp9.test20008.com'
module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'ii-deais-nuxtjs',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Nuxt.js project' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  plugins: [
      '~/plugins/pluginsvue-scrollto.js', // ここを追加
      {src:'~/plugins/commonjquery.js',ssr:false}
  ],
  /*
  ** Build configuration
  */
  build: {
    vendor:['jquery'],
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  },
  generate: {
    interval: 1000, //インターバルないとサーバーが落ちます。
    // routes () {
    //   return Promise.all([
    //     axios.get(`${apiUrl}/wp-json/wp/v2/posts`),
    //   ]).then((data) => {
    //     const posts = data[0]
    //     return posts.data.map((post) => {
    //       return {
    //         route: '/testpost/' + post.id,
    //         payload: post
    //       }
    //     })
    //   })
    // }

    async routes() {
        const { data: posts } = await axios.get('https://wp9.test20008.com/wp-json/wp/v2/posts');
        const { data: category } = await axios.get('http://wp9.test20008.com/wp-json/wp/v2/categories');
        const post1Page = posts.map(post => `/testpost/${post.id}`);
        const post2Page = category.map(post => `/category/${post.id}`);
        return [...post1Page, ...post2Page];
      },

  }
}

